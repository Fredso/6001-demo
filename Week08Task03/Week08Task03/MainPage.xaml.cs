﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Week08Task03
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            var colors = new List<string> { "red", "green", "blue", "yellow" };
            SelectColor.ItemsSource = colors;
        }

        private void SelectColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var color = new SolidColorBrush(Windows.UI.Color.FromArgb(ColorTransformation(SelectColor.SelectedIndex)[0], ColorTransformation(SelectColor.SelectedIndex)[1], ColorTransformation(SelectColor.SelectedIndex)[2], ColorTransformation(SelectColor.SelectedIndex)[3]));
            MySecondGrid.Background = color;
        }
        private List<byte> ColorTransformation(int selection)
        {
            var selectedcolor = new List<byte> { 0, 0, 0, 0 };
            switch (selection)
            {
                case 0:
                    selectedcolor[0]= 255;
                    selectedcolor[1]= 255;
                    selectedcolor[2]= 0;
                    selectedcolor[3]= 0;
                    break;
                case 1:
                    selectedcolor[0] = 255;
                    selectedcolor[1] = 0;
                    selectedcolor[2] = 255;
                    selectedcolor[3] = 0;
                    break;
                case 2:
                    selectedcolor[0] = 255;
                    selectedcolor[1] = 0;
                    selectedcolor[2] = 0;
                    selectedcolor[3] = 255;
                    break;
                case 3:
                    selectedcolor[0] = 255;
                    selectedcolor[1] = 255;
                    selectedcolor[2] = 255;
                    selectedcolor[3] = 150;
                    break;
            }
            return selectedcolor;
        }
    }
}
