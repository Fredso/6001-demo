﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace WFPTest03
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>

    public enum Type { Vegetal, Fruit, Grain, Bread, Pastry, Sauce, Meat, Fish, Dairy, Sweet }
    public enum Production { Natural, Manufactured }
    public class Food
    {
        public string name;
        public Type type;
        public Production production;
        public double gramsconsumed;
        public double calories;
        public double sugarcontent;
        public double saltcontent;

        public Food()
        {
            name = "apple";
            type = Type.Fruit;
            production = Production.Natural;
            gramsconsumed = 100.0;
            calories = 52.0;
            sugarcontent = 10.0;
            saltcontent = 0.001;
        }
        public Food(string _name, Type _type, Production _production, double _gramsconsumed, double _calories, double _sugarcontent, double _saltcontent)
        {
            name = _name;
            type = _type;
            production = _production;
            gramsconsumed = _gramsconsumed;
            calories = _calories;
            sugarcontent = _sugarcontent;
            saltcontent = _saltcontent;
        }
    }

    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();


            var FoodSelection = 0;

            //It creates two instances from the class Food as a base
            Food food1 = new Food("banana", Type.Fruit, Production.Natural, 100.0, 89.0, 12.0, 0.001);
            Food food2 = new Food("toast", Type.Bread, Production.Manufactured, 100.0, 313.0, 6.0, 0.601);

            //It creates a list of food which includes the instances of the food clases
            var ListofFoods = new List<Food> { food1, food2 };

            var ConsumedCalories = 0.0;
            var ConsumedSugar = 0.0;
            var ConsumedSalt = 0.0;

            //It pastes the list of foods to the combobox
            cbFoodSelection.ItemsSource = GetFoodNames(ListofFoods);
        

        }

        //It extracts and returns the names of the foods from the list of class instances into a list of strings
        static List<string> GetFoodNames(List<Food> myList)
        {
            var ListofFoodNames = new List<string> { };
            foreach (Food food in myList)
            {
                ListofFoodNames.Add(food.name);
            }
            return ListofFoodNames;
        }

        private void cbFoodSelection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selection(cbFoodSelection.SelectedValue.ToString());
        }

        public string selection( string str)
        {
            var FoodSelection = str;

            return FoodSelection;
        }
    }
}
